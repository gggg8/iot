#include <Wire.h>

int ls = 0;
unsigned char id = 1;

void setup() {
  
  Wire.begin(id);
  Wire.onReceive(funAnswer);
  Serial.begin(9600);
}

struct listSlave {
  char num;
  int address;};

listSlave adrSlavers[] = {{'S1', 1},{'S2', 2},{'S3', 3}};

int sa = 0;

char receiveName(int addr) {
  for (auto& address : adrSlavers) {
    if (address.address == addr) {
        return address.num;
    }
}
}
void funAnswer(int length) {
  ls = Wire.read();
  sa = ls;
  Serial.write("From: ");
  Serial.write(receiveName(ls));

  while (Wire.available()) {
    char c = Wire.read();
    Serial.print(c);
  }
  Serial.println();
}

void loop() {
 
  if (Serial.available()) {
    char com_input = Serial.read();
    
    if (com_input == 'r') {
      if (sa != 0) {
        Serial.print("Message to last sender\n");
        Wire.beginTransmission(sa);
        delay(100);
        Wire.write(id);
        while (Serial.available()) {
          char c = Serial.read();
          Serial.write(c);
          Wire.write(c);
        }
        Wire.endTransmission();
        Serial.write("\ndelivered\n");
      } else {
        Serial.println("mistake\n");
      }
    } else {
      int payee_adr = -1;
      for (int i = 0; i < sizeof(adrSlavers) / sizeof(adrSlavers[0]); i++) {
        if (adrSlavers[i].num == com_input) {
          payee_adr = adrSlavers[i].address;
          break;
        }
      }

      if (payee_adr != -1) {
        Serial.print("To: ");
        Serial.write(com_input);
        
        Wire.beginTransmission(payee_adr);
        delay(100);
        Wire.write(id);
        while (Serial.available()) {
          char c = Serial.read();
          Serial.write(c);
          Wire.write(c);
        }
        Wire.endTransmission();
        Serial.write("\ndelivered.\n");
      } else {
        Serial.println("mistale\n");
      }
    }
  }
}