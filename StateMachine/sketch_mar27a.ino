
#include <NewPing.h>

#define SPEED_1      5 
#define DIR_1        4 
#define SPEED_2      6 
#define DIR_2        7 

#define TRIG_PIN_RIGHT 10
#define ECHO_PIN_RIGHT 11

#define TRIG_PIN_FORWARD 6
#define ECHO_PIN_FORWARD 7

#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters)

#define CAR_SPEED 255 // Speed for moving forward
#define TURN_SPEED 128 // Speed for turning

NewPing rightSensor(TRIG_PIN_RIGHT, ECHO_PIN_RIGHT, MAX_DISTANCE); // Right sensor
NewPing forwardSensor(TRIG_PIN_FORWARD, ECHO_PIN_FORWARD, MAX_DISTANCE); // Forward sensor

void setup() {
  Serial.begin(9600);
  
  pinMode(SPEED_1, OUTPUT);
  pinMode(DIR_1, OUTPUT);
  pinMode(SPEED_2, OUTPUT);
  pinMode(DIR_2, OUTPUT);

  pinMode(TRIG_PIN_RIGHT, OUTPUT); // Устанавливаем пин TRIG_PIN_RIGHT в режим вывода
  pinMode(ECHO_PIN_RIGHT, INPUT);  // Устанавливаем пин ECHO_PIN_RIGHT в режим ввода
  pinMode(TRIG_PIN_FORWARD, OUTPUT); // Устанавливаем пин TRIG_PIN_FORWARD в режим вывода
  pinMode(ECHO_PIN_FORWARD, INPUT);
}

void move_forward() {
  digitalWrite(DIR_1, HIGH);  
  digitalWrite(DIR_2, HIGH);
  analogWrite(SPEED_1, CAR_SPEED);  
  analogWrite(SPEED_2, CAR_SPEED);
}

void rotate_left() {
  digitalWrite(DIR_1, HIGH);  
  digitalWrite(DIR_2, LOW);
  analogWrite(SPEED_1, TURN_SPEED);  
  analogWrite(SPEED_2, TURN_SPEED);
}

void rotate_right() {
  digitalWrite(DIR_1, LOW);  
  digitalWrite(DIR_2, HIGH);
  analogWrite(SPEED_1, TURN_SPEED);  
  analogWrite(SPEED_2, TURN_SPEED);
}

void stop() {
  analogWrite(SPEED_1, 0);  
  analogWrite(SPEED_2, 0);
}

void loop() {
  int rightDistance = rightSensor.ping_cm(); // Расстояние от стены справа
  int forwardDistance = forwardSensor.ping_cm(); // Расстояние от препятствия спереди

  // Проверяем, есть ли препятствие спереди
  if (forwardDistance < 10) {
    // Если есть препятствие спереди, уклоняемся влево
    Serial.println(forwardDistance);
    Serial.println("FORWARD");
    Serial.println("Обнаружено препятствие спереди. Уклоняемся вправо.");
    rotate_left();
    delay(500); // Задержка для поворота
    stop();
  } else {
    // Если нет препятствия спереди, двигаемся вперед
    Serial.println("Препятствий спереди нет. Двигаемся вперед.");
    move_forward();
  }

  // Проверяем, есть ли стена справа
  if (rightDistance < 10) {
    // Если стена справа слишком близко, уклоняемся влево
    Serial.println(rightDistance);
    Serial.println("RIGHT");
    Serial.println("Стена справа слишком близко. Уклоняемся влево.");
    rotate_left();
    delay(500); // Задержка для поворота
    stop();
  } else if (rightDistance > 15) {
    // Если стена справа слишком далеко, уклоняемся вправо
    Serial.println(rightDistance);
    Serial.println("RIGHT");
    Serial.println("Стена справа слишком далеко. Уклоняемся вправо.");
    rotate_right();
    delay(500); // Задержка для поворота
    stop();
  } else {
    // Если стена справа на нужном расстоянии, двигаемся вперед
    Serial.println("Стена справа на нужном расстоянии. Двигаемся вперед.");
    move_forward();
  }
}
