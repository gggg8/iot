const int btn1_pin = 2;
bool btn1;
bool bounce_btn1 = 0;
bool buttonWasUp = true;
bool ledEnabled = false;
unsigned long lastDebounceTime = 0;
unsigned long debounceDelay = 50; // задержка дребезга в миллисекундах
unsigned long holdStartTime = 0;
int brightness = 0;
int fadeAmount = 5;

void setup() {
  pinMode(10, OUTPUT);
  pinMode(2, INPUT_PULLUP);
  Serial.begin(9600);
}

void loop() {
  boolean buttonIsUp = digitalRead(2);
  
  if (buttonWasUp && !buttonIsUp) {
    lastDebounceTime = millis();
    holdStartTime = millis(); // начало удержания кнопки
  }
  
  if ((millis() - lastDebounceTime) > debounceDelay) {
    buttonIsUp = digitalRead(2);
    
    if (!buttonIsUp) {
      unsigned long holdTime = millis() - holdStartTime;
      
      if (holdTime > 1000) { // Если удерживается менее 1 секунды
        brightness = map(holdTime, 0, 1000, 0, 255);
      } else {
        brightness = 255; // Максимальная яркость после 1 секунды удержания
      }
      
      analogWrite(10, brightness);
    } else {
      ledEnabled = !ledEnabled;
      digitalWrite(10, ledEnabled ? HIGH : LOW);
      brightness = 0; // Сброс яркости при отпускании кнопки
    }
    
    buttonWasUp = buttonIsUp;
  }
}
