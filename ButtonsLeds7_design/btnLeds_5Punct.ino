#define START_PIN 3
#define END_PIN 10
#define BUTTON_PIN 2 
#define BOUNCE_TIME 50
#define PRESSED LOW

bool digits[10][8] = {
  {1,1,0,1,1,1,0,1},  // 0
  {0,1,0,1,0,0,0,0},  // 1
  {1,1,0,0,1,1,1,0},  // 2
  {1,1,0,1,1,0,1,0},  // 3
  {0,1,0,1,0,0,1,1},  // 4
  {1,0,0,1,1,0,1,1},  // 5
  {1,0,1,1,1,1,1,1},  // 6
  {1,1,0,1,0,0,0,0},  // 7
  {1,1,0,1,1,1,1,1},  // 8
  {1,1,1,1,1,0,1,1}   // 9
};

volatile bool check_button = false;
volatile long int press_time = 0;
volatile int holding_time = 0;

void setup() {
  for(int i = START_PIN; i <= END_PIN; i++) {
    pinMode(i, OUTPUT);
    digitalWrite(i, HIGH);
  }
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  Serial.begin(9600);

  // Присоед. прерывание к BUTTON_PIN с прерыванием по фронту сигнала
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), process_button_click, RISING);
}

void loop() {
  if(check_button && (millis() - press_time > BOUNCE_TIME) && (digitalRead(BUTTON_PIN) == PRESSED)) {
    // и деления на 3000 мс для перевода
    holding_time = (millis() - press_time + 2999) / 3000;

    show_digit(holding_time % 10);
    Serial.println("Pressed button for");
    Serial.println(holding_time);

    // Сброс нажатия кнопки
    check_button = false;
  }
}

void process_button_click() {
  if (check_button == false) {
    press_time = millis();
    check_button = true;
  }
}

void show_digit(int digit) {
  if (digit<0 or digit > 9) {
    return;
  }
  for(int i=START_PIN; i <=END_PIN; i++) {
    digitalWrite(i, !digits[digit][i-START_PIN]);
  }
}
