#define pin2 2 
#define pin3 3
#define pin4 4
#define pin5 5

const int nRows = 2;
const int nCols = 2;
int rPins[nRows] = {pin2, pin3};
int cPins[nCols] = {pin4, pin5};

void setup()
{
  for (int i = 0; i < nRows; i++) {
    pinMode(rPins[i], OUTPUT);
    digitalWrite(rPins[i], HIGH);
  }
  
  for (int i = 0; i < nCols; i++) {
    pinMode(cPins[i], INPUT_PULLUP);
  }
  
  Serial.begin(9600);
}

void loop()
{
  for (int row = 0; row < nRows; row++) {
    digitalWrite(rPins[row], LOW);
    
    for (int col = 0; col < nCols; col++) {
      if (digitalRead(cPins[col]) == LOW) {
        Serial.print("pressed: ");
        Serial.print(row * nCols + col + 1);

        delay(100);
      
        for (int r = 0; r < nRows; r++) {
          digitalWrite(rPins[r], LOW);
          
          for (int c = 0; c < nCols; c++) {
            if (digitalRead(cPins[c]) == LOW && (r != row || c != col)) {
              Serial.print(" and ");
              Serial.print(r * nCols + c + 1);
            }
          }
          
          digitalWrite(rPins[r], HIGH);
        }
        Serial.println();
      }
    }
    digitalWrite(rPins[row], HIGH);
    delay(10);
  }
}